package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {
    private int key;

    public CaesarTransformation(int key) {
        this.key = key;
    }

    public CaesarTransformation() {
        this(13);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        StringBuilder sb = new StringBuilder();
        for(char c : text.toCharArray()) {
            if (Character.isLetter(c)) {
                if       (c >= 'a' && c <= 'm') c += key;
                else if  (c >= 'A' && c <= 'M') c += key;
                else if  (c >= 'n' && c <= 'z') c -= key;
                else if  (c >= 'N' && c <= 'Z') c -= key;
            }
            sb.append(c);
        }
        return new Spell(sb.toString(), codex);
    }
}
