package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean justDidLargeSpell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.justDidLargeSpell = false;
    }

    @Override
    public String normalAttack() {
        justDidLargeSpell = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (justDidLargeSpell) {
            return "Can't cast large spell twice in a row!";
        } else {
            justDidLargeSpell = true;
            return spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
